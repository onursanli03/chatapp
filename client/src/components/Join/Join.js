import React, { useState } from 'react';
import { Link } from "react-router-dom";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import { makeStyles } from '@material-ui/core/styles';

import './Join.css';
import { FormControl } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(0),
    minWidth: '100%'
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function SignIn() {
  const [name, setName] = useState('');
  const [room, setRoom] = useState('');
  const classes = useStyles();

  const handleChange = (event) => {
    setRoom(event.target.value);
  };

  return (
    <div className="joinOuterContainer">
      <div className="joinInnerContainer">
        <h1 className="heading">Join Chatroom</h1>
        <div>
          <TextField className="joinInput" label="Nickname" onChange={(event) => setName(event.target.value)} />
        </div>
        <div>
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="select" id="demo-simple-select-label">Select Room</InputLabel>
            <Select
              fullWidth
              id="select"
              labelId="demo-simple-select-label"
              value={room}
              onChange={handleChange}
              className="joinSelect"
            >
              <MenuItem value=""><em>None</em></MenuItem>
              <MenuItem value={'general'}>General</MenuItem>
              <MenuItem value={'architecture'}>Architecture</MenuItem>
              <MenuItem value={'travel'}>Travel</MenuItem>
              <MenuItem value={'relationships'}>Relationships</MenuItem>
            </Select>
          </FormControl>
        </div>
        <Link onClick={e => (!name || !room) ? e.preventDefault() : null} to={`/chat?name=${name}&room=${room}`}>
          <button className={'button mt-20'} type="submit">Sign In</button>
        </Link>
      </div>
    </div>
  );
}