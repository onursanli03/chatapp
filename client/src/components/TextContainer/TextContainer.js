import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import onlineIcon from '../../icons/onlineIcon.png';

import './TextContainer.css';

const TextContainer = ({ users }) => (
  <div className="textContainer">
    <div>
      <h1>
        <span role="img" aria-label="emoji">⚠️</span> 
          Chat Rules 
        <span role="img" aria-label="emoji">⚠️</span>
      </h1>
      <List>
          <ListItem>
            <ListItemText
              primary="Be kind"
            />
          </ListItem>
          <ListItem>
            <ListItemText
              primary="No judgement"
            />
          </ListItem>
          <ListItem>
            <ListItemText
              primary="Please support each other"
            />
          </ListItem>
          <ListItem>
            <ListItemText
              primary="Please follow the rules. If someone break the rules, please let me know"
            />
          </ListItem>
      </List>
    </div>
    {
      users
        ? (
          <div>
            <h1>People currently chatting:</h1>
            <div className="activeContainer">
              <h2>
                {users.map(({ name }) => (
                  <div key={name} className="activeItem">
                    <img className="textImg" alt="Online Icon" src={onlineIcon} />
                    {name}
                  </div>
                ))}
              </h2>
            </div>
          </div>
        )
        : null
    }
  </div>
);

export default TextContainer;