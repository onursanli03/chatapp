const http = require('http');
const express = require('express');
const socketio = require('socket.io');
const cors = require('cors');
const got = require('got');

const { addUser, removeUser, getUser, getUsersInRoom } = require('./users');

const router = require('./router');
const chatLog = 'Human: Hello, who are you?\nAI: I am doing great. How can I help you today?\n';
const app = express();
const server = http.createServer(app);
const io = socketio(server);

app.use(cors());
app.use(router);

io.on('connect', (socket) => {
  socket.on('join', ({ name, room }, callback) => {
    const { error, user } = addUser({ id: socket.id, name, room });

    if(error) return callback(error);

    socket.join(user.room);

    socket.emit('message', { user: 'admin', text: `${user.name}, welcome to room ${user.room}.`});
    socket.broadcast.to(user.room).emit('message', { user: 'admin', text: `${user.name} has joined!` });

    io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room) });

    callback();
  });

  socket.on('sendMessage', (message, callback) => {
    const user = getUser(socket.id);

    io.to(user.room).emit('message', { user: user.name, text: message });
    getOpenAiResponse(message,user);
    callback();
  });

  socket.on('disconnect', () => {
    const user = removeUser(socket.id);

    if(user) {
      io.to(user.room).emit('message', { user: 'Admin', text: `${user.name} has left.` });
      io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room)});
    }
  });
});

async function getOpenAiResponse(message, user){
  const url = 'https://api.openai.com/v1/engines/davinci/completions';
  const prompt = `${chatLog}Human: ${message}`;
  const params = {
    'prompt': prompt,
    'max_tokens': 400,
    'temperature': 0.9,
    'frequency_penalty': 0,
    'presence_penalty': 0.6,
    'best_of': 1,
    'top_p': 1,
    'stop': '\nHuman'
  };
  const headers = {
    'Authorization': 'Bearer sk-l48MXtbKVg3w4HAaQ9RqSRDxTXT9zrr2WOP7JamF',
    'Content-Type': 'application/json'
    //'Authorization': `Bearer ${process.env.OPENAI_SECRET_KEY}`,
  };

  try {
    const response = await got.post(url, { json: params, headers: headers }).json();
    output = `${response.choices[0].text.split(':')[1]}`;
    io.to(user.room).emit('message', { user: 'Ai', text: output.toString() });
    console.log(output.toString());
  } catch (err) {
    console.log(err);
  }
}

server.listen(process.env.PORT || 5000, () => console.log(`Server has started.`));